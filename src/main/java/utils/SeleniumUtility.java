package main.java.utils;

/**
 * @author lionel.mangoua
 * date: 18/02/22
 */

import io.qameta.allure.Allure;
import junit.framework.Assert;
import main.java.engine.DriverFactory;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.ByteArrayInputStream;

public class SeleniumUtility extends DriverFactory {

    public static int WaitTimeout = 6;

    //region <attachScreenShot>
    /**
     * To take screenshot
     * and log to Allure Report
     */
    public static void attachScreenShot(String description) {

        try {
            Allure.addAttachment(description, new ByteArrayInputStream(((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES))); // get screenshot from somewhere
        }
        catch(Exception e) {
            log("Failed to attach screenshot --- " + e.getMessage(), "ERROR",  "text");
        }
    }
    //endregion

    //region <clearTextByXpath>
    public static void clearTextByXpath(String elementXpath, String errorMessage) {

        try {
            waitForElementByXpath(elementXpath, errorMessage);
            WebElement elementToTypeIn = driver.findElement(By.xpath(elementXpath));
            elementToTypeIn.clear();

            log("Cleared text on element : " + elementXpath,"INFO",  "text");
            Assert.assertTrue("Cleared text on element : " + elementXpath, true);
        }
        catch (Exception e) {
            log(errorMessage, "ERROR",  "text");
            attachScreenShot("\n[ERROR] Failed to clear text on element --- " + elementXpath); //take screenshot when action fails
            Assert.fail("\n[ERROR] Failed to clear text on element --- " + elementXpath + " - " + e.getMessage());
        }
    }
    //endregion

    //region <clickElementByXpath>
    public static void clickElementByXpath(String elementXpath, String errorMessage) {

        try {
            waitForElementByXpath(elementXpath, errorMessage);
            WebDriverWait wait = new WebDriverWait(driver, WaitTimeout);
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath(elementXpath)));
            WebElement elementToClick = driver.findElement(By.xpath(elementXpath));
            elementToClick.click();

            log("Clicked element by Xpath : " + elementXpath,"INFO",  "text");
            Assert.assertTrue("Clicked element by Xpath : " + elementXpath, true);
        }
        catch (Exception e) {
            log(errorMessage, "ERROR",  "text");
            attachScreenShot("\n[ERROR] Failed to click on element by Xpath  ---  " + elementXpath); //take screenshot when action fails
            Assert.fail("\n[ERROR] Failed to click on element by Xpath  ---  " + elementXpath + "' - " + e.getMessage());
        }
    }
    //endregion

    //region <enterTextByXpath>
    public static void enterTextByXpath(String elementXpath, String textToEnter, String errorMessage) {

        try {
            if (textToEnter.equals("")) {
                log("There is No text to enter","INFO",  "text");
                Assert.assertTrue("There is No text to enter", true);
            }
            else if(textToEnter.equalsIgnoreCase("Clear")) {
                waitForElementByXpath(elementXpath, errorMessage);
                WebElement elementToTypeIn = driver.findElement(By.xpath(elementXpath));
                elementToTypeIn.clear();
                Assert.assertTrue("Cleared text field", true);
            }
            else {
                waitForElementByXpath(elementXpath, errorMessage);
                Actions action = new Actions(driver);
                WebElement elementToTypeIn = driver.findElement(By.xpath(elementXpath));
                elementToTypeIn.click();
                action.keyDown(Keys.CONTROL).sendKeys("a").keyUp(Keys.CONTROL).perform();
                elementToTypeIn.sendKeys(textToEnter);

                log("Entered text : \"" + textToEnter + "\" to : " + elementXpath,"INFO",  "text");
                Assert.assertTrue("Entered text : \"" + textToEnter + "\" to : " + elementXpath, true);
            }
        }
        catch (Exception e) {
            log(errorMessage, "ERROR",  "text");
            attachScreenShot("\n[ERROR] Failed to enter text : \"" + textToEnter + "\" by Xpath  ---  " + elementXpath);
            Assert.fail("\n[ERROR] Failed to enter text : \"" + textToEnter + "\" by Xpath  ---  " + elementXpath + "' - " + e.getMessage());
        }
    }
    //endregion

    //region <extractTextByXpath>
    public static String extractTextByXpath(String elementXpath, String errorMessage) {

        String retrievedText = "";
        try {
            WebElement elementToRead = driver.findElement(By.xpath(elementXpath));
            retrievedText = elementToRead.getText();

            log("Extracted text: '" + retrievedText + "' retrieved from element --- " + elementXpath, "ERROR",  "text");
            Assert.assertTrue("Extracted text: " + retrievedText + " retrieved from element --- " + elementXpath, true);

            return retrievedText;
        }
        catch (Exception e) {
            log(errorMessage, "ERROR",  "text");
            attachScreenShot("\n[ERROR] Failed to retrieve text from element --- " + elementXpath);
            Assert.fail("\n[ERROR] Failed to retrieve text from element --- " + elementXpath + " - " + e.getMessage());

            return retrievedText;
        }
    }
    //endregion

    //region <pause>
    public static void pause(int millisecondsWait) {

        try {
            Thread.sleep(millisecondsWait);
            log("Paused for " + millisecondsWait + " milliseconds successfully", "ERROR",  "text");
        }
        catch (Exception e) {
            log("Failed to pause", "ERROR",  "text");
            Assert.fail("\n[ERROR] Failed to pause --- " + e.getMessage());
        }
    }
    //endregion

    //region <waitForElementByXpath>
    public static void waitForElementByXpath(String elementXpath, String errorMessage) {

        boolean elementFound = false;
        try {
            int waitCount = 0;
            while (!elementFound && waitCount < WaitTimeout) {
                try {
                    WebDriverWait wait = new WebDriverWait(driver, 1);

                    wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(elementXpath)));
                    wait.until(ExpectedConditions.elementToBeClickable(By.xpath(elementXpath)));
                    if (wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(elementXpath))) != null) {
                        elementFound = true;
                        log("Found element by Xpath : " + elementXpath,"INFO",  "text");
                        Assert.assertTrue("Found element by Xpath : " + elementXpath, true);
                        break;
                    }
                }
                catch (Exception e) {
                    elementFound = false;
                    log("Did Not Find element by Xpath: " + elementXpath, "ERROR",  "text");
                }
                waitCount++;
            }
            if (waitCount == WaitTimeout) {
                GetElementFound1(elementFound);
                log(errorMessage, "ERROR",  "text");
                attachScreenShot("\n[ERROR] Reached TimeOut whilst waiting for element by Xpath: '" + elementXpath);
                Assert.fail("\n[ERROR] Reached TimeOut whilst waiting for element by Xpath: '" + elementXpath + "'");
            }
        }
        catch (Exception e) {
            log(errorMessage, "ERROR",  "text");
            attachScreenShot("\n[ERROR] Failed to wait for element by Xpath --- " + elementXpath);
            Assert.fail("\n[ERROR] Failed to wait for element by Xpath --- " + elementXpath + "' - " + e.getMessage());
        }

        GetElementFound(elementFound);
    }

    private static boolean GetElementFound(boolean elementFound) {
        return elementFound;
    }

    private static boolean GetElementFound1(boolean elementFound) {
        return elementFound;
    }

    private static boolean returnElementFound(boolean elementFound) {
        return elementFound;
    }
    //endregion

    //region <waitForElementToBeLocatedByXpath>
    public static void waitForElementToBeLocatedByXpath(String elementXpath, Integer timeout, String errorMessage) {

        boolean elementFound = false;
        try {
            WebDriverWait wait = new WebDriverWait(driver, timeout);
            wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(elementXpath)));

            elementFound = true;
            log("Found element : " + elementXpath,"INFO",  "text");
        }
        catch(Exception e) {
            returnElementFound(elementFound);
            log(errorMessage, "ERROR",  "text");
            attachScreenShot("\n[ERROR] Reached TimeOut whilst waiting for element --- " + elementXpath); //take screenshot when action fails
            Assert.fail("\n[ERROR] Reached TimeOut whilst waiting for element --- " + elementXpath);
        }

        returnElementFound(true);
    }
    //endregion
}
