package main.java.engine;

/**
 * @author lionel.mangoua
 * date: 18/02/22
 */

import io.appium.java_client.android.AndroidDriver;
import io.github.bonigarcia.wdm.WebDriverManager;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;
import io.restassured.specification.FilterableRequestSpecification;
import io.restassured.specification.FilterableResponseSpecification;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import main.java.utils.PropertyFileReader;
import main.java.utils.SeleniumUtility;
import org.json.JSONException;
import org.json.JSONObject;
import org.openqa.selenium.UnexpectedAlertBehaviour;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

import java.util.concurrent.TimeUnit;

public class DriverFactory {

    public static PropertyFileReader property = new PropertyFileReader();
    public static String web_fileName = "WEB";
    public static String api_fileName = "API";
    public static String mobi_fileName = "MOBILE";
    //Headers
    public static String contentTypeJson = property.returnPropVal_api(api_fileName, "content_type_json");
    public static String contentTypeXml = property.returnPropVal_api(api_fileName, "content_type_xml");
    //Api
    public static ValidatableResponse response = null;
    public static Response res = null;
    public static RequestSpecBuilder builder;
    public static RequestSpecification requestSpec; //used with ValidatableResponse
    public static FilterableRequestSpecification requestSpecification; //used with Response
    public static ResponseSpecBuilder respec; //used with ValidatableResponse
    public static ResponseSpecification responseSpec; //used with ValidatableResponse
    public static FilterableResponseSpecification responseSpecification; //used with Response
    public static String api_url = property.returnPropVal_api(api_fileName, "url");
    //Web
    public static RemoteWebDriver driver;
    public static WebDriverWait wait = null;
    public static final int WAIT_TIME = 30;
    public static String BROWSER = System.getProperty("BROWSER");
    public static ChromeOptions options;
    public static final Logger LOGGER = LoggerFactory.getLogger(DriverFactory.class);
    public static String web_url = property.returnPropVal_web(web_fileName, "url");
    //Mobile
    public static AndroidDriver Driver; //For appium
    public static DesiredCapabilities capabilities;
    public static String appPath = "src/main/resources/mobile/xxx.apk"; //For mobile app
    public static String appAbsolutePath = "";
    public static String label = property.returnPropVal_mobi(mobi_fileName, "label");

    @BeforeSuite(alwaysRun = true)
    public void initialiseDriverAndProperties() {

        setupLocalDriver();
    }

    //WEB
    //region <setupLocalDriver>
    public static void setupLocalDriver() {

        log("\nWeb test is Starting ... \n","INFO",  "text");

        if("Firefox".equals(BROWSER)) {
            //setup the firefoxdriver using WebDriverManager dependency
            WebDriverManager.firefoxdriver().setup();

            FirefoxOptions firefoxOptions = new FirefoxOptions();

            driver = new FirefoxDriver(firefoxOptions);
        }
        else if("Chrome".equals(BROWSER)) {
            //setup the chromedriver using WebDriverManager dependency
            WebDriverManager.chromedriver().setup();

            options = new ChromeOptions();

            options.addArguments("--disable-extensions");
            options.addArguments("disable-infobars");
            options.addArguments("test-type");
            options.addArguments("enable-strict-powerful-feature-restrictions");
            options.addArguments("--disable-popup-bloacking");
            options.addArguments("--dns-prefetch-disable");
            options.addArguments("--safebrowsing-disable-auto-update");
            options.addArguments("disable-restore-session-state");
            options.addArguments("start-maximized"); //maximize browser
            options.setCapability(ChromeOptions.CAPABILITY, options);
            options.setCapability(CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR, UnexpectedAlertBehaviour.IGNORE);

            driver = new ChromeDriver(options);
        }

        log("\n'" + BROWSER + "' browser on local machine initiated \n","INFO",  "text");

        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        wait = new WebDriverWait(driver, WAIT_TIME);
    }
    //endregion

    //region <logger>
    public static void logger(final String message, final String level, String format) {

        if(format.equalsIgnoreCase(("json"))) {
            String json = (new JSONObject(message)).toString(4); //To convert into pretty Json format
            LOGGER.info("\n" + json); //To print on the console
        }
        else {
            LOGGER.info(message); //To print on the console
        }
    }

    public static void log(final String message, final String level, String format) {

        try {
            logger(message, level, format);
        }
        catch (JSONException err) {
            logger(message, level, "text");
        }
    }
    //endregion

    @AfterSuite(alwaysRun = true)
    //region <tearDown>
    public void tearDown() {

        try {
            if(driver != null) {
                //To check if the browser is opened
                LOGGER.info("Test - WEB is Ending ...\n");
                SeleniumUtility.attachScreenShot("Tear down screenshot - Web");
                driver.quit();
            }

            if(Driver != null) {
                //To check if the mobile app is opened
                LOGGER.info("Test - MOBILE is Ending ...\n");
                SeleniumUtility.attachScreenShot("Tear down screenshot - Mobile");
                Driver.closeApp(); //Close the app which was provided in the capabilities at session creation
                Driver.quit(); //Quit the session created between the client and the server
            }
        }
        catch(Exception ex) {
            log("Something went wrong on test suite tear down!!! ---> " + ex, "INFO", "text");
        }
    }
    //endregion
}
